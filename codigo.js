
function guardarDatos() {
  //Funcion capturar datos
	const nombre = document.getElementById('nombre').value;
	const apellido = document.getElementById('apellido').value;
  const numero = document.getElementById('numero').value;
	const datosKey = 'datos';

	const dato = {
		nombre,
		apellido,
		numero
	}

	let datos = JSON.parse(localStorage.getItem(datosKey));
	if (datos && datos.length > 0) {
		datos.push(dato);
	}else {
		datos = []
		datos.push(dato);
	}
	localStorage.setItem(datosKey, JSON.stringify(datos));


	document.getElementById('nombre').value = "";
	document.getElementById('apellido').value = "";
	document.getElementById('numero').value = "";


}
